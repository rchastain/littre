/********************************************************************************
** Form generated from reading UI file 'classeapropos.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLASSEAPROPOS_H
#define UI_CLASSEAPROPOS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_fenetreAPropos
{
public:
    QLabel *etiquetteTitre;
    QTabWidget *ongletPrincipal;
    QWidget *pageLicence;
    QVBoxLayout *verticalLayout;
    QTextBrowser *navigateurLicence;
    QWidget *pageCredits;
    QVBoxLayout *vboxLayout;
    QTextBrowser *navigateurCredits;
    QPushButton *boutonFermer;
    QProgressBar *barreProgression;
    QLabel *etiquetteFond;

    void setupUi(QDialog *fenetreAPropos)
    {
        if (fenetreAPropos->objectName().isEmpty())
            fenetreAPropos->setObjectName(QString::fromUtf8("fenetreAPropos"));
        fenetreAPropos->resize(278, 386);
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Serif"));
        font.setPointSize(10);
        fenetreAPropos->setFont(font);
        etiquetteTitre = new QLabel(fenetreAPropos);
        etiquetteTitre->setObjectName(QString::fromUtf8("etiquetteTitre"));
        etiquetteTitre->setGeometry(QRect(0, 0, 278, 36));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QLinearGradient gradient(0, 0, 0, 1);
        gradient.setSpread(QGradient::PadSpread);
        gradient.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient.setColorAt(0, QColor(119, 119, 119, 255));
        gradient.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush1(gradient);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QLinearGradient gradient1(0, 0, 0, 1);
        gradient1.setSpread(QGradient::PadSpread);
        gradient1.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient1.setColorAt(0, QColor(119, 119, 119, 255));
        gradient1.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient1.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient1.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush2(gradient1);
        palette.setBrush(QPalette::Active, QPalette::Base, brush2);
        QLinearGradient gradient2(0, 0, 0, 1);
        gradient2.setSpread(QGradient::PadSpread);
        gradient2.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient2.setColorAt(0, QColor(119, 119, 119, 255));
        gradient2.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient2.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient2.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush3(gradient2);
        palette.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        QLinearGradient gradient3(0, 0, 0, 1);
        gradient3.setSpread(QGradient::PadSpread);
        gradient3.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient3.setColorAt(0, QColor(119, 119, 119, 255));
        gradient3.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient3.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient3.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush4(gradient3);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush4);
        QLinearGradient gradient4(0, 0, 0, 1);
        gradient4.setSpread(QGradient::PadSpread);
        gradient4.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient4.setColorAt(0, QColor(119, 119, 119, 255));
        gradient4.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient4.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient4.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush5(gradient4);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush5);
        QLinearGradient gradient5(0, 0, 0, 1);
        gradient5.setSpread(QGradient::PadSpread);
        gradient5.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient5.setColorAt(0, QColor(119, 119, 119, 255));
        gradient5.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient5.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient5.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush6(gradient5);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush6);
        QBrush brush7(QColor(127, 125, 123, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush7);
        QLinearGradient gradient6(0, 0, 0, 1);
        gradient6.setSpread(QGradient::PadSpread);
        gradient6.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient6.setColorAt(0, QColor(119, 119, 119, 255));
        gradient6.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient6.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient6.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush8(gradient6);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush8);
        QLinearGradient gradient7(0, 0, 0, 1);
        gradient7.setSpread(QGradient::PadSpread);
        gradient7.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient7.setColorAt(0, QColor(119, 119, 119, 255));
        gradient7.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient7.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient7.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush9(gradient7);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush9);
        QLinearGradient gradient8(0, 0, 0, 1);
        gradient8.setSpread(QGradient::PadSpread);
        gradient8.setCoordinateMode(QGradient::ObjectBoundingMode);
        gradient8.setColorAt(0, QColor(119, 119, 119, 255));
        gradient8.setColorAt(0.4, QColor(96, 96, 96, 255));
        gradient8.setColorAt(0.401, QColor(87, 87, 87, 255));
        gradient8.setColorAt(1, QColor(51, 51, 51, 255));
        QBrush brush10(gradient8);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush10);
        etiquetteTitre->setPalette(palette);
        QFont font1;
        font1.setPointSize(12);
        etiquetteTitre->setFont(font1);
        etiquetteTitre->setAlignment(Qt::AlignCenter);
        ongletPrincipal = new QTabWidget(fenetreAPropos);
        ongletPrincipal->setObjectName(QString::fromUtf8("ongletPrincipal"));
        ongletPrincipal->setGeometry(QRect(9, 45, 260, 285));
        ongletPrincipal->setUsesScrollButtons(false);
        pageLicence = new QWidget();
        pageLicence->setObjectName(QString::fromUtf8("pageLicence"));
        verticalLayout = new QVBoxLayout(pageLicence);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        navigateurLicence = new QTextBrowser(pageLicence);
        navigateurLicence->setObjectName(QString::fromUtf8("navigateurLicence"));
        QFont font2;
        font2.setPointSize(9);
        navigateurLicence->setFont(font2);
        navigateurLicence->setFrameShape(QFrame::NoFrame);
        navigateurLicence->setOpenExternalLinks(true);

        verticalLayout->addWidget(navigateurLicence);

        ongletPrincipal->addTab(pageLicence, QString());
        pageCredits = new QWidget();
        pageCredits->setObjectName(QString::fromUtf8("pageCredits"));
        vboxLayout = new QVBoxLayout(pageCredits);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        navigateurCredits = new QTextBrowser(pageCredits);
        navigateurCredits->setObjectName(QString::fromUtf8("navigateurCredits"));
        navigateurCredits->setFont(font2);
        navigateurCredits->setFrameShape(QFrame::NoFrame);
        navigateurCredits->setOpenExternalLinks(true);

        vboxLayout->addWidget(navigateurCredits);

        ongletPrincipal->addTab(pageCredits, QString());
        boutonFermer = new QPushButton(fenetreAPropos);
        boutonFermer->setObjectName(QString::fromUtf8("boutonFermer"));
        boutonFermer->setGeometry(QRect(148, 339, 121, 38));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icones/X.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonFermer->setIcon(icon);
        boutonFermer->setIconSize(QSize(30, 30));
        boutonFermer->setAutoDefault(false);
        barreProgression = new QProgressBar(fenetreAPropos);
        barreProgression->setObjectName(QString::fromUtf8("barreProgression"));
        barreProgression->setGeometry(QRect(0, 353, 278, 24));
        barreProgression->setTextVisible(false);
        etiquetteFond = new QLabel(fenetreAPropos);
        etiquetteFond->setObjectName(QString::fromUtf8("etiquetteFond"));
        etiquetteFond->setGeometry(QRect(0, 36, 278, 350));
        etiquetteFond->setPixmap(QPixmap(QString::fromUtf8(":/images/Portrait d'Emile Littre.png")));
        etiquetteFond->raise();
        etiquetteTitre->raise();
        ongletPrincipal->raise();
        boutonFermer->raise();
        barreProgression->raise();
        QWidget::setTabOrder(boutonFermer, ongletPrincipal);
        QWidget::setTabOrder(ongletPrincipal, navigateurLicence);
        QWidget::setTabOrder(navigateurLicence, navigateurCredits);

        retranslateUi(fenetreAPropos);
        QObject::connect(boutonFermer, SIGNAL(clicked()), fenetreAPropos, SLOT(close()));

        ongletPrincipal->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(fenetreAPropos);
    } // setupUi

    void retranslateUi(QDialog *fenetreAPropos)
    {
        fenetreAPropos->setWindowTitle(QApplication::translate("fenetreAPropos", "\303\200 propos du logiciel", nullptr));
        etiquetteTitre->setStyleSheet(QApplication::translate("fenetreAPropos", "background-color: qlineargradient(x1: 0, y1: 0, x2: 0.0, y2: 1.0, stop: 0 #777777, stop: 0.4 #606060, stop: 0.401 #575757, stop: 1 #333333);", nullptr));
        etiquetteTitre->setText(QApplication::translate("fenetreAPropos", "Dictionnaire le Littr\303\251 2.1", nullptr));
        navigateurLicence->setHtml(QApplication::translate("fenetreAPropos", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"Licence\"></a>\302\251 Murielle Descerisiers, 2009 <br /><a href=\"mailto:murielle.descerisiers@gmail.com\"><span style=\" text-decoration: underline; color:#0000ff;\">murielle.descerisiers@gmail.com</span></a> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Ce logiciel est un programme informatique d\303\251velopp\303\251 au Qu\303\251bec et servant \303\240 consulter le dictionnaire de la langue fran\303\247aise d'\303\211mile Littr\303\251. </p>\n"
"<p style=\" ma"
                        "rgin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Ce logiciel est r\303\251gi par la licence CeCILL soumise au droit fran\303\247ais et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL telle que diffus\303\251e par le CEA, le CNRS et l'INRIA sur le site <a href=\"http://www.cecill.info\"><span style=\" text-decoration: underline; color:#0000ff;\">http://www.cecill.info</span></a>. </p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">En contrepartie de l'accessibilit\303\251 au code source et des droits de copie, de modification et de redistribution accord\303\251s par cette licence, il n'est offert aux utilisateurs qu'une garantie limit\303\251e. Pour les m\303\252mes raisons, seule une responsabilit\303\251 restreinte p\303\250se sur l'auteur du programme, le t"
                        "itulaire des droits patrimoniaux et les conc\303\251dants successifs. </p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\303\200 cet \303\251gard l'attention de l'utilisateur est attir\303\251e sur les risques associ\303\251s au chargement, \303\240 l'utilisation, \303\240 la modification et/ou au d\303\251veloppement et \303\240 la reproduction du logiciel par l'utilisateur \303\251tant donn\303\251 sa sp\303\251cificit\303\251 de logiciel libre, qui peut le rendre complexe \303\240 manipuler et qui le r\303\251serve donc \303\240 des d\303\251veloppeurs et des professionnels avertis poss\303\251dant des connaissances informatiques approfondies. Les utilisateurs sont donc invit\303\251s \303\240 charger et tester l'ad\303\251quation du logiciel \303\240 leurs besoins dans des conditions permettant d'assurer la s\303\251curit\303\251 de leurs syst\303\250mes et ou de leurs donn\303\251es et, plus g\303\251n\303\251ralement, \303\240"
                        " l'utiliser et l'exploiter dans les m\303\252mes conditions de s\303\251curit\303\251. </p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Le fait que vous puissiez acc\303\251der \303\240 cet en-t\303\252te signifie que vous avez pris connaissance de la licence CeCILL, et que vous en avez accept\303\251 les termes.</p></body></html>", nullptr));
        ongletPrincipal->setTabText(ongletPrincipal->indexOf(pageLicence), QApplication::translate("fenetreAPropos", "&Licence", nullptr));
        navigateurCredits->setHtml(QApplication::translate("fenetreAPropos", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"Cr\303\251dits\"></a>Logiciel \302\253 Dictionnaire le Littr\303\251 \302\273 par Murielle Descerisiers <br /><a href=\"http://dictionnaire-le-littre.googlecode.com/\"><span style=\" text-decoration: underline; color:#0000ff;\">http://dictionnaire-le-littre.googlecode.com/</span></a> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Base de donn\303\251es \302\253 XMLittr\303\251 \302\273 par Fran\303\247ois Gannaz <br /><a href=\"http://francois.gannaz.free.fr/Littre/"
                        "\"><span style=\" text-decoration: underline; color:#0000ff;\">http://francois.gannaz.free.fr/Littre/</span></a> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Ic\303\264nes \302\253 Buuf \302\273 et \302\253 Buuf Deuce \302\273 par Paul Davey <br /><a href=\"http://mattahan.deviantart.com/\"><span style=\" text-decoration: underline; color:#0000ff;\">http://mattahan.deviantart.com/</span></a> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Police \302\253 DejaVu Serif \302\273 par \305\240t\304\233p\303\241n Roh <br /><a href=\"http://dejavu-fonts.org/\"><span style=\" text-decoration: underline; color:#0000ff;\">http://dejavu-fonts.org/</span></a></p></body></html>", nullptr));
        ongletPrincipal->setTabText(ongletPrincipal->indexOf(pageCredits), QApplication::translate("fenetreAPropos", "&Cr\303\251dits", nullptr));
        boutonFermer->setText(QApplication::translate("fenetreAPropos", "&Fermer", nullptr));
        etiquetteFond->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class fenetreAPropos: public Ui_fenetreAPropos {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLASSEAPROPOS_H
