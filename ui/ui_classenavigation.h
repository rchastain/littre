/********************************************************************************
** Form generated from reading UI file 'classenavigation.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLASSENAVIGATION_H
#define UI_CLASSENAVIGATION_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_fenetreNavigation
{
public:
    QAction *actionConsulter;
    QPushButton *boutonAnnuler;
    QPushButton *boutonConsulter;
    QListWidget *zoneListeCorrespondances;
    QLabel *etiquette;

    void setupUi(QDialog *fenetreNavigation)
    {
        if (fenetreNavigation->objectName().isEmpty())
            fenetreNavigation->setObjectName(QString::fromUtf8("fenetreNavigation"));
        fenetreNavigation->resize(371, 185);
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Serif"));
        font.setPointSize(10);
        fenetreNavigation->setFont(font);
        actionConsulter = new QAction(fenetreNavigation);
        actionConsulter->setObjectName(QString::fromUtf8("actionConsulter"));
        boutonAnnuler = new QPushButton(fenetreNavigation);
        boutonAnnuler->setObjectName(QString::fromUtf8("boutonAnnuler"));
        boutonAnnuler->setGeometry(QRect(101, 138, 121, 38));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icones/X.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonAnnuler->setIcon(icon);
        boutonAnnuler->setIconSize(QSize(30, 30));
        boutonAnnuler->setAutoDefault(false);
        boutonConsulter = new QPushButton(fenetreNavigation);
        boutonConsulter->setObjectName(QString::fromUtf8("boutonConsulter"));
        boutonConsulter->setGeometry(QRect(231, 138, 131, 38));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icones/Reused Hand.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonConsulter->setIcon(icon1);
        boutonConsulter->setIconSize(QSize(30, 30));
        boutonConsulter->setAutoDefault(false);
        zoneListeCorrespondances = new QListWidget(fenetreNavigation);
        zoneListeCorrespondances->setObjectName(QString::fromUtf8("zoneListeCorrespondances"));
        zoneListeCorrespondances->setGeometry(QRect(9, 33, 353, 96));
        zoneListeCorrespondances->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        zoneListeCorrespondances->setAlternatingRowColors(true);
        zoneListeCorrespondances->setUniformItemSizes(true);
        etiquette = new QLabel(fenetreNavigation);
        etiquette->setObjectName(QString::fromUtf8("etiquette"));
        etiquette->setGeometry(QRect(9, 9, 353, 24));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        etiquette->setFont(font1);
        etiquette->setAlignment(Qt::AlignCenter);
#ifndef QT_NO_SHORTCUT
        etiquette->setBuddy(zoneListeCorrespondances);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(zoneListeCorrespondances, boutonConsulter);
        QWidget::setTabOrder(boutonConsulter, boutonAnnuler);

        retranslateUi(fenetreNavigation);
        QObject::connect(boutonAnnuler, SIGNAL(clicked()), fenetreNavigation, SLOT(close()));

        QMetaObject::connectSlotsByName(fenetreNavigation);
    } // setupUi

    void retranslateUi(QDialog *fenetreNavigation)
    {
        fenetreNavigation->setWindowTitle(QApplication::translate("fenetreNavigation", "Navigation", nullptr));
        actionConsulter->setText(QApplication::translate("fenetreNavigation", "Consulter", nullptr));
        boutonAnnuler->setText(QApplication::translate("fenetreNavigation", "&Annuler", nullptr));
        boutonConsulter->setText(QApplication::translate("fenetreNavigation", "&Consulter", nullptr));
        etiquette->setText(QApplication::translate("fenetreNavigation", "&Liste des correspondances", nullptr));
    } // retranslateUi

};

namespace Ui {
    class fenetreNavigation: public Ui_fenetreNavigation {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLASSENAVIGATION_H
