/********************************************************************************
** Form generated from reading UI file 'classerecherche.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLASSERECHERCHE_H
#define UI_CLASSERECHERCHE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_fenetreRecherche
{
public:
    QLabel *etiquetteResultats;
    QListWidget *zoneListeResultats;
    QPushButton *boutonImprimer;
    QPushButton *boutonExporter;
    QPushButton *boutonAfficherApercuImpression;
    QProgressBar *barreProgression;
    QToolButton *boutonChercher;
    QToolButton *boutonModeChamp;
    QComboBox *zoneTexteRecherche;
    QPushButton *boutonConsulter;
    QPushButton *boutonFermerOuAnnuler;

    void setupUi(QDialog *fenetreRecherche)
    {
        if (fenetreRecherche->objectName().isEmpty())
            fenetreRecherche->setObjectName(QString::fromUtf8("fenetreRecherche"));
        fenetreRecherche->resize(439, 475);
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Serif"));
        font.setPointSize(10);
        fenetreRecherche->setFont(font);
        etiquetteResultats = new QLabel(fenetreRecherche);
        etiquetteResultats->setObjectName(QString::fromUtf8("etiquetteResultats"));
        etiquetteResultats->setGeometry(QRect(9, 56, 421, 24));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(etiquetteResultats->sizePolicy().hasHeightForWidth());
        etiquetteResultats->setSizePolicy(sizePolicy);
        etiquetteResultats->setMinimumSize(QSize(0, 24));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        etiquetteResultats->setFont(font1);
        etiquetteResultats->setAlignment(Qt::AlignCenter);
        zoneListeResultats = new QListWidget(fenetreRecherche);
        zoneListeResultats->setObjectName(QString::fromUtf8("zoneListeResultats"));
        zoneListeResultats->setEnabled(false);
        zoneListeResultats->setGeometry(QRect(9, 80, 421, 340));
        zoneListeResultats->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        zoneListeResultats->setEditTriggers(QAbstractItemView::NoEditTriggers);
        zoneListeResultats->setUniformItemSizes(true);
        boutonImprimer = new QPushButton(fenetreRecherche);
        boutonImprimer->setObjectName(QString::fromUtf8("boutonImprimer"));
        boutonImprimer->setGeometry(QRect(49, 429, 38, 38));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icones/Printer.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonImprimer->setIcon(icon);
        boutonImprimer->setIconSize(QSize(32, 32));
        boutonImprimer->setAutoDefault(false);
        boutonExporter = new QPushButton(fenetreRecherche);
        boutonExporter->setObjectName(QString::fromUtf8("boutonExporter"));
        boutonExporter->setGeometry(QRect(9, 429, 38, 38));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icones/You take forever to say nothing..png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonExporter->setIcon(icon1);
        boutonExporter->setIconSize(QSize(32, 32));
        boutonExporter->setAutoDefault(false);
        boutonAfficherApercuImpression = new QPushButton(fenetreRecherche);
        boutonAfficherApercuImpression->setObjectName(QString::fromUtf8("boutonAfficherApercuImpression"));
        boutonAfficherApercuImpression->setGeometry(QRect(89, 429, 38, 38));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icones/Printer Script...or Pript.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonAfficherApercuImpression->setIcon(icon2);
        boutonAfficherApercuImpression->setIconSize(QSize(32, 32));
        boutonAfficherApercuImpression->setAutoDefault(false);
        barreProgression = new QProgressBar(fenetreRecherche);
        barreProgression->setObjectName(QString::fromUtf8("barreProgression"));
        barreProgression->setGeometry(QRect(9, 16, 421, 24));
        boutonChercher = new QToolButton(fenetreRecherche);
        boutonChercher->setObjectName(QString::fromUtf8("boutonChercher"));
        boutonChercher->setGeometry(QRect(352, 9, 38, 38));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icones/Golden Binoculars.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonChercher->setIcon(icon3);
        boutonChercher->setIconSize(QSize(32, 32));
        boutonModeChamp = new QToolButton(fenetreRecherche);
        boutonModeChamp->setObjectName(QString::fromUtf8("boutonModeChamp"));
        boutonModeChamp->setGeometry(QRect(392, 9, 38, 38));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icones/U.B.K. Eternal Sledge Hammer.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonModeChamp->setIcon(icon4);
        boutonModeChamp->setIconSize(QSize(32, 32));
        boutonModeChamp->setPopupMode(QToolButton::InstantPopup);
        zoneTexteRecherche = new QComboBox(fenetreRecherche);
        zoneTexteRecherche->setObjectName(QString::fromUtf8("zoneTexteRecherche"));
        zoneTexteRecherche->setGeometry(QRect(9, 10, 341, 36));
        zoneTexteRecherche->setEditable(true);
        boutonConsulter = new QPushButton(fenetreRecherche);
        boutonConsulter->setObjectName(QString::fromUtf8("boutonConsulter"));
        boutonConsulter->setEnabled(false);
        boutonConsulter->setGeometry(QRect(289, 429, 141, 38));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icones/Reused Hand.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonConsulter->setIcon(icon5);
        boutonConsulter->setIconSize(QSize(32, 32));
        boutonConsulter->setAutoDefault(false);
        boutonFermerOuAnnuler = new QPushButton(fenetreRecherche);
        boutonFermerOuAnnuler->setObjectName(QString::fromUtf8("boutonFermerOuAnnuler"));
        boutonFermerOuAnnuler->setGeometry(QRect(159, 429, 121, 38));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/icones/X.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonFermerOuAnnuler->setIcon(icon6);
        boutonFermerOuAnnuler->setIconSize(QSize(32, 32));
        boutonFermerOuAnnuler->setAutoDefault(false);
        boutonChercher->raise();
        boutonModeChamp->raise();
        zoneTexteRecherche->raise();
        boutonConsulter->raise();
        boutonFermerOuAnnuler->raise();
        barreProgression->raise();
        etiquetteResultats->raise();
        zoneListeResultats->raise();
        boutonImprimer->raise();
        boutonExporter->raise();
        boutonAfficherApercuImpression->raise();
#ifndef QT_NO_SHORTCUT
        etiquetteResultats->setBuddy(zoneTexteRecherche);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(zoneTexteRecherche, boutonChercher);
        QWidget::setTabOrder(boutonChercher, boutonModeChamp);
        QWidget::setTabOrder(boutonModeChamp, zoneListeResultats);
        QWidget::setTabOrder(zoneListeResultats, boutonConsulter);
        QWidget::setTabOrder(boutonConsulter, boutonFermerOuAnnuler);
        QWidget::setTabOrder(boutonFermerOuAnnuler, boutonExporter);
        QWidget::setTabOrder(boutonExporter, boutonImprimer);
        QWidget::setTabOrder(boutonImprimer, boutonAfficherApercuImpression);

        retranslateUi(fenetreRecherche);

        QMetaObject::connectSlotsByName(fenetreRecherche);
    } // setupUi

    void retranslateUi(QDialog *fenetreRecherche)
    {
        fenetreRecherche->setWindowTitle(QApplication::translate("fenetreRecherche", "Chercher dans tous les articles", nullptr));
        etiquetteResultats->setText(QApplication::translate("fenetreRecherche", "&R\303\251sultats de la recherche", nullptr));
#ifndef QT_NO_TOOLTIP
        boutonImprimer->setToolTip(QApplication::translate("fenetreRecherche", "Imprimer les r\303\251sultats...", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        boutonExporter->setToolTip(QApplication::translate("fenetreRecherche", "Exporter les r\303\251sultats", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        boutonAfficherApercuImpression->setToolTip(QApplication::translate("fenetreRecherche", "Afficher l'aper\303\247u avant impression...", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        boutonChercher->setToolTip(QApplication::translate("fenetreRecherche", "Chercher", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        boutonModeChamp->setToolTip(QApplication::translate("fenetreRecherche", "Mode et champ", nullptr));
#endif // QT_NO_TOOLTIP
        boutonConsulter->setText(QApplication::translate("fenetreRecherche", "&Consulter", nullptr));
        boutonFermerOuAnnuler->setText(QApplication::translate("fenetreRecherche", "&Fermer", nullptr));
    } // retranslateUi

};

namespace Ui {
    class fenetreRecherche: public Ui_fenetreRecherche {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLASSERECHERCHE_H
