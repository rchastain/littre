/********************************************************************************
** Form generated from reading UI file 'classeprincipale.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLASSEPRINCIPALE_H
#define UI_CLASSEPRINCIPALE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_fenetrePrincipale
{
public:
    QAction *actionConsulter;
    QAction *actionImprimer;
    QAction *actionQuitter;
    QAction *actionCopier;
    QAction *actionToutSelectionner;
    QAction *actionOccurrenceSuivante;
    QAction *actionOccurrencePrecedente;
    QAction *actionChercherTousLesArticles;
    QAction *actionReculerHistorique;
    QAction *actionAvancerHistorique;
    QAction *actionReculerEntrees;
    QAction *actionAvancerEntrees;
    QAction *actionAgrandirTexte;
    QAction *actionReduireTexte;
    QAction *actionAProposDuLogiciel;
    QAction *actionLirePreface;
    QAction *actionAfficherPleinEcran;
    QAction *actionLireCauserie;
    QAction *actionExporterHTML;
    QAction *actionExporterODF;
    QAction *actionExporterTXT;
    QAction *actionExporterBalises;
    QAction *actionAfficherPortrait;
    QAction *actionAfficherFlexion;
    QAction *actionNegligerAccents;
    QAction *actionDefilerAFurEtAMesure;
    QAction *actionEmployerExpressionRationnelle;
    QAction *actionRespecterCasse;
    QAction *actionChangerTypographie;
    QAction *actionAfficherApercuImpression;
    QAction *actionExporterPDF;
    QAction *actionInclurePrononciation;
    QAction *actionInclureNature;
    QAction *actionInclureCitations;
    QAction *actionInclureHistorique;
    QAction *actionInclureEtymologie;
    QAction *action_Zola_parle_d_mile_Littr;
    QAction *actionRe_nan_d_mile;
    QAction *actionLireTemoignagePasteur;
    QAction *actionLireTemoignageRenan;
    QAction *actionLireTemoignageZola;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_2;
    QSplitter *splitter;
    QWidget *cadreGauche;
    QVBoxLayout *verticalLayout;
    QTabWidget *ongletConsultationEtRecherche;
    QWidget *pageConsultation;
    QHBoxLayout *horizontalLayout;
    QLineEdit *zoneTexteConsultation;
    QToolButton *boutonConsulter;
    QToolButton *boutonModeConsultation;
    QWidget *pageRechercheDansArticle;
    QHBoxLayout *horizontalLayout_2;
    QComboBox *zoneTexteRechercheDansArticle;
    QToolButton *boutonOccurrenceSuivante;
    QToolButton *boutonOccurrencePrecedente;
    QToolButton *boutonModeRecherche;
    QTabWidget *ongletPrincipal;
    QWidget *pageNomenclature;
    QVBoxLayout *verticalLayout_3;
    QLabel *etiquetteNomenclature;
    QTabWidget *ongletNomenclature;
    QWidget *pageEntrees;
    QVBoxLayout *vboxLayout;
    QListView *zoneListeEntrees;
    QWidget *pageFormesFlechies;
    QVBoxLayout *vboxLayout1;
    QListView *zoneListeFormesFlechies;
    QWidget *pageSurvol;
    QVBoxLayout *vboxLayout2;
    QLabel *etiquetteSurvol;
    QListWidget *zoneListeSurvol;
    QWidget *pageHistorique;
    QVBoxLayout *vboxLayout3;
    QLabel *etiquetteHistorique;
    QListWidget *zoneListeHistorique;
    QFrame *cadreDroit;
    QHBoxLayout *horizontalLayout_3;
    QTextBrowser *navigateurArticle;
    QWidget *widget_3;
    QToolButton *boutonAvancerHistorique;
    QToolButton *boutonReculerHistorique;
    QToolButton *boutonAgrandirTexte;
    QToolButton *boutonReduireTexte;
    QToolButton *boutonExporter;
    QToolButton *boutonImprimer;
    QToolButton *boutonCopier;
    QToolButton *boutonAfficherPleinEcran;
    QToolButton *boutonAfficherFlexion;
    QToolButton *boutonChercherTousLesArticles;
    QToolButton *boutonChampLecture;
    QMenuBar *menuBarre;
    QMenu *menuFichier;
    QMenu *menuExporter;
    QMenu *menuExporterBalises;
    QMenu *menuEdition;
    QMenu *menuNavigation;
    QMenu *menuOptions;
    QMenu *menuModeConsultation;
    QMenu *menuModeRecherche;
    QMenu *menuChampLecture;
    QMenu *menuAide;
    QMenu *menuLireTemoignage;
    QMenu *menuRecherche;
    QMenu *menuChercherArticle;

    void setupUi(QMainWindow *fenetrePrincipale)
    {
        if (fenetrePrincipale->objectName().isEmpty())
            fenetrePrincipale->setObjectName(QString::fromUtf8("fenetrePrincipale"));
        fenetrePrincipale->resize(750, 550);
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Serif"));
        font.setPointSize(10);
        fenetrePrincipale->setFont(font);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icones/Girls I do adore....png"), QSize(), QIcon::Normal, QIcon::Off);
        fenetrePrincipale->setWindowIcon(icon);
        actionConsulter = new QAction(fenetrePrincipale);
        actionConsulter->setObjectName(QString::fromUtf8("actionConsulter"));
        actionImprimer = new QAction(fenetrePrincipale);
        actionImprimer->setObjectName(QString::fromUtf8("actionImprimer"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icones/Printer.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionImprimer->setIcon(icon1);
        actionQuitter = new QAction(fenetrePrincipale);
        actionQuitter->setObjectName(QString::fromUtf8("actionQuitter"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icones/X.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionQuitter->setIcon(icon2);
        actionCopier = new QAction(fenetrePrincipale);
        actionCopier->setObjectName(QString::fromUtf8("actionCopier"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icones/Documents.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCopier->setIcon(icon3);
        actionToutSelectionner = new QAction(fenetrePrincipale);
        actionToutSelectionner->setObjectName(QString::fromUtf8("actionToutSelectionner"));
        actionOccurrenceSuivante = new QAction(fenetrePrincipale);
        actionOccurrenceSuivante->setObjectName(QString::fromUtf8("actionOccurrenceSuivante"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icones/Right Down.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOccurrenceSuivante->setIcon(icon4);
        actionOccurrencePrecedente = new QAction(fenetrePrincipale);
        actionOccurrencePrecedente->setObjectName(QString::fromUtf8("actionOccurrencePrecedente"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icones/Left Up.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOccurrencePrecedente->setIcon(icon5);
        actionChercherTousLesArticles = new QAction(fenetrePrincipale);
        actionChercherTousLesArticles->setObjectName(QString::fromUtf8("actionChercherTousLesArticles"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/icones/Golden Binoculars.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionChercherTousLesArticles->setIcon(icon6);
        actionReculerHistorique = new QAction(fenetrePrincipale);
        actionReculerHistorique->setObjectName(QString::fromUtf8("actionReculerHistorique"));
        actionReculerHistorique->setEnabled(false);
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/icones/Previous.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionReculerHistorique->setIcon(icon7);
        actionAvancerHistorique = new QAction(fenetrePrincipale);
        actionAvancerHistorique->setObjectName(QString::fromUtf8("actionAvancerHistorique"));
        actionAvancerHistorique->setEnabled(false);
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/icones/Next.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAvancerHistorique->setIcon(icon8);
        actionReculerEntrees = new QAction(fenetrePrincipale);
        actionReculerEntrees->setObjectName(QString::fromUtf8("actionReculerEntrees"));
        actionReculerEntrees->setEnabled(false);
        actionAvancerEntrees = new QAction(fenetrePrincipale);
        actionAvancerEntrees->setObjectName(QString::fromUtf8("actionAvancerEntrees"));
        actionAvancerEntrees->setEnabled(false);
        actionAgrandirTexte = new QAction(fenetrePrincipale);
        actionAgrandirTexte->setObjectName(QString::fromUtf8("actionAgrandirTexte"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/icones/Zoom In.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAgrandirTexte->setIcon(icon9);
        actionReduireTexte = new QAction(fenetrePrincipale);
        actionReduireTexte->setObjectName(QString::fromUtf8("actionReduireTexte"));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/icones/Zoom Out. Machinie Gun Funk Baby!.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionReduireTexte->setIcon(icon10);
        actionAProposDuLogiciel = new QAction(fenetrePrincipale);
        actionAProposDuLogiciel->setObjectName(QString::fromUtf8("actionAProposDuLogiciel"));
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/icones/Info.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAProposDuLogiciel->setIcon(icon11);
        actionLirePreface = new QAction(fenetrePrincipale);
        actionLirePreface->setObjectName(QString::fromUtf8("actionLirePreface"));
        actionAfficherPleinEcran = new QAction(fenetrePrincipale);
        actionAfficherPleinEcran->setObjectName(QString::fromUtf8("actionAfficherPleinEcran"));
        actionAfficherPleinEcran->setCheckable(true);
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/icones/Sewy.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAfficherPleinEcran->setIcon(icon12);
        actionLireCauserie = new QAction(fenetrePrincipale);
        actionLireCauserie->setObjectName(QString::fromUtf8("actionLireCauserie"));
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/icones/Toy House.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLireCauserie->setIcon(icon13);
        actionExporterHTML = new QAction(fenetrePrincipale);
        actionExporterHTML->setObjectName(QString::fromUtf8("actionExporterHTML"));
        actionExporterODF = new QAction(fenetrePrincipale);
        actionExporterODF->setObjectName(QString::fromUtf8("actionExporterODF"));
        actionExporterTXT = new QAction(fenetrePrincipale);
        actionExporterTXT->setObjectName(QString::fromUtf8("actionExporterTXT"));
        actionExporterBalises = new QAction(fenetrePrincipale);
        actionExporterBalises->setObjectName(QString::fromUtf8("actionExporterBalises"));
        actionAfficherPortrait = new QAction(fenetrePrincipale);
        actionAfficherPortrait->setObjectName(QString::fromUtf8("actionAfficherPortrait"));
        actionAfficherFlexion = new QAction(fenetrePrincipale);
        actionAfficherFlexion->setObjectName(QString::fromUtf8("actionAfficherFlexion"));
        actionAfficherFlexion->setEnabled(false);
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/icones/Can't forget the De La.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAfficherFlexion->setIcon(icon14);
        actionNegligerAccents = new QAction(fenetrePrincipale);
        actionNegligerAccents->setObjectName(QString::fromUtf8("actionNegligerAccents"));
        actionNegligerAccents->setCheckable(true);
        actionDefilerAFurEtAMesure = new QAction(fenetrePrincipale);
        actionDefilerAFurEtAMesure->setObjectName(QString::fromUtf8("actionDefilerAFurEtAMesure"));
        actionDefilerAFurEtAMesure->setCheckable(true);
        actionDefilerAFurEtAMesure->setChecked(true);
        actionEmployerExpressionRationnelle = new QAction(fenetrePrincipale);
        actionEmployerExpressionRationnelle->setObjectName(QString::fromUtf8("actionEmployerExpressionRationnelle"));
        actionEmployerExpressionRationnelle->setCheckable(true);
        actionRespecterCasse = new QAction(fenetrePrincipale);
        actionRespecterCasse->setObjectName(QString::fromUtf8("actionRespecterCasse"));
        actionRespecterCasse->setCheckable(true);
        actionChangerTypographie = new QAction(fenetrePrincipale);
        actionChangerTypographie->setObjectName(QString::fromUtf8("actionChangerTypographie"));
        QIcon icon15;
        icon15.addFile(QString::fromUtf8(":/icones/Pages.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionChangerTypographie->setIcon(icon15);
        actionAfficherApercuImpression = new QAction(fenetrePrincipale);
        actionAfficherApercuImpression->setObjectName(QString::fromUtf8("actionAfficherApercuImpression"));
        QIcon icon16;
        icon16.addFile(QString::fromUtf8(":/icones/Printer Script...or Pript.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAfficherApercuImpression->setIcon(icon16);
        actionExporterPDF = new QAction(fenetrePrincipale);
        actionExporterPDF->setObjectName(QString::fromUtf8("actionExporterPDF"));
        actionInclurePrononciation = new QAction(fenetrePrincipale);
        actionInclurePrononciation->setObjectName(QString::fromUtf8("actionInclurePrononciation"));
        actionInclurePrononciation->setCheckable(true);
        actionInclurePrononciation->setChecked(true);
        actionInclureNature = new QAction(fenetrePrincipale);
        actionInclureNature->setObjectName(QString::fromUtf8("actionInclureNature"));
        actionInclureNature->setCheckable(true);
        actionInclureNature->setChecked(true);
        actionInclureCitations = new QAction(fenetrePrincipale);
        actionInclureCitations->setObjectName(QString::fromUtf8("actionInclureCitations"));
        actionInclureCitations->setCheckable(true);
        actionInclureCitations->setChecked(true);
        actionInclureHistorique = new QAction(fenetrePrincipale);
        actionInclureHistorique->setObjectName(QString::fromUtf8("actionInclureHistorique"));
        actionInclureHistorique->setCheckable(true);
        actionInclureHistorique->setChecked(true);
        actionInclureEtymologie = new QAction(fenetrePrincipale);
        actionInclureEtymologie->setObjectName(QString::fromUtf8("actionInclureEtymologie"));
        actionInclureEtymologie->setCheckable(true);
        actionInclureEtymologie->setChecked(true);
        action_Zola_parle_d_mile_Littr = new QAction(fenetrePrincipale);
        action_Zola_parle_d_mile_Littr->setObjectName(QString::fromUtf8("action_Zola_parle_d_mile_Littr"));
        actionRe_nan_d_mile = new QAction(fenetrePrincipale);
        actionRe_nan_d_mile->setObjectName(QString::fromUtf8("actionRe_nan_d_mile"));
        actionLireTemoignagePasteur = new QAction(fenetrePrincipale);
        actionLireTemoignagePasteur->setObjectName(QString::fromUtf8("actionLireTemoignagePasteur"));
        actionLireTemoignageRenan = new QAction(fenetrePrincipale);
        actionLireTemoignageRenan->setObjectName(QString::fromUtf8("actionLireTemoignageRenan"));
        actionLireTemoignageZola = new QAction(fenetrePrincipale);
        actionLireTemoignageZola->setObjectName(QString::fromUtf8("actionLireTemoignageZola"));
        centralwidget = new QWidget(fenetrePrincipale);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_2 = new QVBoxLayout(centralwidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        splitter = new QSplitter(centralwidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        cadreGauche = new QWidget(splitter);
        cadreGauche->setObjectName(QString::fromUtf8("cadreGauche"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(cadreGauche->sizePolicy().hasHeightForWidth());
        cadreGauche->setSizePolicy(sizePolicy);
        cadreGauche->setMinimumSize(QSize(0, 80));
        verticalLayout = new QVBoxLayout(cadreGauche);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        ongletConsultationEtRecherche = new QTabWidget(cadreGauche);
        ongletConsultationEtRecherche->setObjectName(QString::fromUtf8("ongletConsultationEtRecherche"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(ongletConsultationEtRecherche->sizePolicy().hasHeightForWidth());
        ongletConsultationEtRecherche->setSizePolicy(sizePolicy1);
        ongletConsultationEtRecherche->setTabPosition(QTabWidget::South);
        ongletConsultationEtRecherche->setUsesScrollButtons(false);
        pageConsultation = new QWidget();
        pageConsultation->setObjectName(QString::fromUtf8("pageConsultation"));
        horizontalLayout = new QHBoxLayout(pageConsultation);
        horizontalLayout->setSpacing(2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        zoneTexteConsultation = new QLineEdit(pageConsultation);
        zoneTexteConsultation->setObjectName(QString::fromUtf8("zoneTexteConsultation"));
        zoneTexteConsultation->setMaximumSize(QSize(16777215, 36));

        horizontalLayout->addWidget(zoneTexteConsultation);

        boutonConsulter = new QToolButton(pageConsultation);
        boutonConsulter->setObjectName(QString::fromUtf8("boutonConsulter"));
        boutonConsulter->setMaximumSize(QSize(38, 38));
        QIcon icon17;
        icon17.addFile(QString::fromUtf8(":/icones/I like Ghostace Killah. He's a top notch emcee and we share the same birthday..png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonConsulter->setIcon(icon17);
        boutonConsulter->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(boutonConsulter);

        boutonModeConsultation = new QToolButton(pageConsultation);
        boutonModeConsultation->setObjectName(QString::fromUtf8("boutonModeConsultation"));
        boutonModeConsultation->setMaximumSize(QSize(38, 38));
        QIcon icon18;
        icon18.addFile(QString::fromUtf8(":/icones/U.B.K. Eternal Sledge Hammer.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonModeConsultation->setIcon(icon18);
        boutonModeConsultation->setIconSize(QSize(32, 32));
        boutonModeConsultation->setPopupMode(QToolButton::InstantPopup);

        horizontalLayout->addWidget(boutonModeConsultation);

        ongletConsultationEtRecherche->addTab(pageConsultation, QString());
        pageRechercheDansArticle = new QWidget();
        pageRechercheDansArticle->setObjectName(QString::fromUtf8("pageRechercheDansArticle"));
        horizontalLayout_2 = new QHBoxLayout(pageRechercheDansArticle);
        horizontalLayout_2->setSpacing(2);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        zoneTexteRechercheDansArticle = new QComboBox(pageRechercheDansArticle);
        zoneTexteRechercheDansArticle->setObjectName(QString::fromUtf8("zoneTexteRechercheDansArticle"));
        sizePolicy1.setHeightForWidth(zoneTexteRechercheDansArticle->sizePolicy().hasHeightForWidth());
        zoneTexteRechercheDansArticle->setSizePolicy(sizePolicy1);
        zoneTexteRechercheDansArticle->setMaximumSize(QSize(16777215, 36));
        zoneTexteRechercheDansArticle->setEditable(true);

        horizontalLayout_2->addWidget(zoneTexteRechercheDansArticle);

        boutonOccurrenceSuivante = new QToolButton(pageRechercheDansArticle);
        boutonOccurrenceSuivante->setObjectName(QString::fromUtf8("boutonOccurrenceSuivante"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(boutonOccurrenceSuivante->sizePolicy().hasHeightForWidth());
        boutonOccurrenceSuivante->setSizePolicy(sizePolicy2);
        boutonOccurrenceSuivante->setMinimumSize(QSize(38, 38));
        boutonOccurrenceSuivante->setMaximumSize(QSize(38, 38));
        boutonOccurrenceSuivante->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(boutonOccurrenceSuivante);

        boutonOccurrencePrecedente = new QToolButton(pageRechercheDansArticle);
        boutonOccurrencePrecedente->setObjectName(QString::fromUtf8("boutonOccurrencePrecedente"));
        sizePolicy2.setHeightForWidth(boutonOccurrencePrecedente->sizePolicy().hasHeightForWidth());
        boutonOccurrencePrecedente->setSizePolicy(sizePolicy2);
        boutonOccurrencePrecedente->setMinimumSize(QSize(38, 38));
        boutonOccurrencePrecedente->setMaximumSize(QSize(38, 38));
        boutonOccurrencePrecedente->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(boutonOccurrencePrecedente);

        boutonModeRecherche = new QToolButton(pageRechercheDansArticle);
        boutonModeRecherche->setObjectName(QString::fromUtf8("boutonModeRecherche"));
        boutonModeRecherche->setMinimumSize(QSize(38, 38));
        boutonModeRecherche->setMaximumSize(QSize(38, 38));
        boutonModeRecherche->setIcon(icon18);
        boutonModeRecherche->setIconSize(QSize(32, 32));
        boutonModeRecherche->setPopupMode(QToolButton::InstantPopup);

        horizontalLayout_2->addWidget(boutonModeRecherche);

        ongletConsultationEtRecherche->addTab(pageRechercheDansArticle, QString());

        verticalLayout->addWidget(ongletConsultationEtRecherche);

        ongletPrincipal = new QTabWidget(cadreGauche);
        ongletPrincipal->setObjectName(QString::fromUtf8("ongletPrincipal"));
        ongletPrincipal->setMinimumSize(QSize(50, 0));
        ongletPrincipal->setTabPosition(QTabWidget::East);
        ongletPrincipal->setIconSize(QSize(28, 28));
        ongletPrincipal->setUsesScrollButtons(false);
        pageNomenclature = new QWidget();
        pageNomenclature->setObjectName(QString::fromUtf8("pageNomenclature"));
        verticalLayout_3 = new QVBoxLayout(pageNomenclature);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        etiquetteNomenclature = new QLabel(pageNomenclature);
        etiquetteNomenclature->setObjectName(QString::fromUtf8("etiquetteNomenclature"));
        etiquetteNomenclature->setMinimumSize(QSize(0, 24));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        etiquetteNomenclature->setFont(font1);
        etiquetteNomenclature->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(etiquetteNomenclature);

        ongletNomenclature = new QTabWidget(pageNomenclature);
        ongletNomenclature->setObjectName(QString::fromUtf8("ongletNomenclature"));
        ongletNomenclature->setTabPosition(QTabWidget::South);
        ongletNomenclature->setUsesScrollButtons(false);
        pageEntrees = new QWidget();
        pageEntrees->setObjectName(QString::fromUtf8("pageEntrees"));
        vboxLayout = new QVBoxLayout(pageEntrees);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        zoneListeEntrees = new QListView(pageEntrees);
        zoneListeEntrees->setObjectName(QString::fromUtf8("zoneListeEntrees"));
        zoneListeEntrees->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        zoneListeEntrees->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        zoneListeEntrees->setAlternatingRowColors(true);
        zoneListeEntrees->setUniformItemSizes(true);

        vboxLayout->addWidget(zoneListeEntrees);

        ongletNomenclature->addTab(pageEntrees, QString());
        pageFormesFlechies = new QWidget();
        pageFormesFlechies->setObjectName(QString::fromUtf8("pageFormesFlechies"));
        vboxLayout1 = new QVBoxLayout(pageFormesFlechies);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        zoneListeFormesFlechies = new QListView(pageFormesFlechies);
        zoneListeFormesFlechies->setObjectName(QString::fromUtf8("zoneListeFormesFlechies"));
        zoneListeFormesFlechies->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        zoneListeFormesFlechies->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        zoneListeFormesFlechies->setAlternatingRowColors(true);
        zoneListeFormesFlechies->setUniformItemSizes(true);

        vboxLayout1->addWidget(zoneListeFormesFlechies);

        ongletNomenclature->addTab(pageFormesFlechies, QString());

        verticalLayout_3->addWidget(ongletNomenclature);

        QIcon icon19;
        icon19.addFile(QString::fromUtf8(":/icones/Library.png"), QSize(), QIcon::Normal, QIcon::Off);
        ongletPrincipal->addTab(pageNomenclature, icon19, QString());
        pageSurvol = new QWidget();
        pageSurvol->setObjectName(QString::fromUtf8("pageSurvol"));
        vboxLayout2 = new QVBoxLayout(pageSurvol);
        vboxLayout2->setSpacing(0);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        etiquetteSurvol = new QLabel(pageSurvol);
        etiquetteSurvol->setObjectName(QString::fromUtf8("etiquetteSurvol"));
        etiquetteSurvol->setMinimumSize(QSize(0, 24));
        etiquetteSurvol->setFont(font1);
        etiquetteSurvol->setAlignment(Qt::AlignCenter);

        vboxLayout2->addWidget(etiquetteSurvol);

        zoneListeSurvol = new QListWidget(pageSurvol);
        zoneListeSurvol->setObjectName(QString::fromUtf8("zoneListeSurvol"));
        zoneListeSurvol->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        zoneListeSurvol->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        zoneListeSurvol->setEditTriggers(QAbstractItemView::NoEditTriggers);
        zoneListeSurvol->setAlternatingRowColors(true);
        zoneListeSurvol->setUniformItemSizes(true);

        vboxLayout2->addWidget(zoneListeSurvol);

        QIcon icon20;
        icon20.addFile(QString::fromUtf8(":/icones/Documents Folder.png"), QSize(), QIcon::Normal, QIcon::Off);
        ongletPrincipal->addTab(pageSurvol, icon20, QString());
        pageHistorique = new QWidget();
        pageHistorique->setObjectName(QString::fromUtf8("pageHistorique"));
        vboxLayout3 = new QVBoxLayout(pageHistorique);
        vboxLayout3->setSpacing(0);
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        etiquetteHistorique = new QLabel(pageHistorique);
        etiquetteHistorique->setObjectName(QString::fromUtf8("etiquetteHistorique"));
        etiquetteHistorique->setMinimumSize(QSize(0, 24));
        etiquetteHistorique->setFont(font1);
        etiquetteHistorique->setAlignment(Qt::AlignCenter);

        vboxLayout3->addWidget(etiquetteHistorique);

        zoneListeHistorique = new QListWidget(pageHistorique);
        zoneListeHistorique->setObjectName(QString::fromUtf8("zoneListeHistorique"));
        zoneListeHistorique->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        zoneListeHistorique->setEditTriggers(QAbstractItemView::NoEditTriggers);
        zoneListeHistorique->setAlternatingRowColors(true);
        zoneListeHistorique->setUniformItemSizes(true);

        vboxLayout3->addWidget(zoneListeHistorique);

        QIcon icon21;
        icon21.addFile(QString::fromUtf8(":/icones/Scheduled Tizasks.png"), QSize(), QIcon::Normal, QIcon::Off);
        ongletPrincipal->addTab(pageHistorique, icon21, QString());

        verticalLayout->addWidget(ongletPrincipal);

        splitter->addWidget(cadreGauche);
        cadreDroit = new QFrame(splitter);
        cadreDroit->setObjectName(QString::fromUtf8("cadreDroit"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(1);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(cadreDroit->sizePolicy().hasHeightForWidth());
        cadreDroit->setSizePolicy(sizePolicy3);
        cadreDroit->setFrameShape(QFrame::StyledPanel);
        horizontalLayout_3 = new QHBoxLayout(cadreDroit);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        navigateurArticle = new QTextBrowser(cadreDroit);
        navigateurArticle->setObjectName(QString::fromUtf8("navigateurArticle"));
        navigateurArticle->setTabChangesFocus(true);
        navigateurArticle->setOpenLinks(false);

        horizontalLayout_3->addWidget(navigateurArticle);

        widget_3 = new QWidget(cadreDroit);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setMinimumSize(QSize(38, 0));
        boutonAvancerHistorique = new QToolButton(widget_3);
        boutonAvancerHistorique->setObjectName(QString::fromUtf8("boutonAvancerHistorique"));
        boutonAvancerHistorique->setGeometry(QRect(0, 40, 38, 38));
        boutonAvancerHistorique->setIconSize(QSize(32, 32));
        boutonReculerHistorique = new QToolButton(widget_3);
        boutonReculerHistorique->setObjectName(QString::fromUtf8("boutonReculerHistorique"));
        boutonReculerHistorique->setGeometry(QRect(0, 0, 38, 38));
        boutonReculerHistorique->setIconSize(QSize(32, 32));
        boutonAgrandirTexte = new QToolButton(widget_3);
        boutonAgrandirTexte->setObjectName(QString::fromUtf8("boutonAgrandirTexte"));
        boutonAgrandirTexte->setGeometry(QRect(0, 120, 38, 38));
        boutonAgrandirTexte->setIconSize(QSize(32, 32));
        boutonReduireTexte = new QToolButton(widget_3);
        boutonReduireTexte->setObjectName(QString::fromUtf8("boutonReduireTexte"));
        boutonReduireTexte->setGeometry(QRect(0, 160, 38, 38));
        boutonReduireTexte->setIconSize(QSize(32, 32));
        boutonExporter = new QToolButton(widget_3);
        boutonExporter->setObjectName(QString::fromUtf8("boutonExporter"));
        boutonExporter->setGeometry(QRect(0, 320, 38, 38));
        boutonExporter->setIconSize(QSize(32, 32));
        boutonExporter->setPopupMode(QToolButton::InstantPopup);
        boutonImprimer = new QToolButton(widget_3);
        boutonImprimer->setObjectName(QString::fromUtf8("boutonImprimer"));
        boutonImprimer->setGeometry(QRect(0, 360, 38, 38));
        boutonImprimer->setIconSize(QSize(32, 32));
        boutonCopier = new QToolButton(widget_3);
        boutonCopier->setObjectName(QString::fromUtf8("boutonCopier"));
        boutonCopier->setGeometry(QRect(0, 280, 38, 38));
        boutonCopier->setIconSize(QSize(32, 32));
        boutonAfficherPleinEcran = new QToolButton(widget_3);
        boutonAfficherPleinEcran->setObjectName(QString::fromUtf8("boutonAfficherPleinEcran"));
        boutonAfficherPleinEcran->setGeometry(QRect(0, 200, 38, 38));
        boutonAfficherPleinEcran->setIconSize(QSize(32, 32));
        boutonAfficherFlexion = new QToolButton(widget_3);
        boutonAfficherFlexion->setObjectName(QString::fromUtf8("boutonAfficherFlexion"));
        boutonAfficherFlexion->setGeometry(QRect(0, 80, 38, 38));
        boutonAfficherFlexion->setIconSize(QSize(32, 32));
        boutonChercherTousLesArticles = new QToolButton(widget_3);
        boutonChercherTousLesArticles->setObjectName(QString::fromUtf8("boutonChercherTousLesArticles"));
        boutonChercherTousLesArticles->setGeometry(QRect(0, 240, 38, 38));
        boutonChercherTousLesArticles->setIconSize(QSize(32, 32));
        boutonChampLecture = new QToolButton(widget_3);
        boutonChampLecture->setObjectName(QString::fromUtf8("boutonChampLecture"));
        boutonChampLecture->setGeometry(QRect(0, 400, 38, 38));
        boutonChampLecture->setIcon(icon18);
        boutonChampLecture->setIconSize(QSize(32, 32));
        boutonChampLecture->setPopupMode(QToolButton::InstantPopup);

        horizontalLayout_3->addWidget(widget_3);

        splitter->addWidget(cadreDroit);

        verticalLayout_2->addWidget(splitter);

        fenetrePrincipale->setCentralWidget(centralwidget);
        menuBarre = new QMenuBar(fenetrePrincipale);
        menuBarre->setObjectName(QString::fromUtf8("menuBarre"));
        menuBarre->setGeometry(QRect(0, 0, 750, 25));
        menuFichier = new QMenu(menuBarre);
        menuFichier->setObjectName(QString::fromUtf8("menuFichier"));
        menuExporter = new QMenu(menuFichier);
        menuExporter->setObjectName(QString::fromUtf8("menuExporter"));
        QIcon icon22;
        icon22.addFile(QString::fromUtf8(":/icones/You take forever to say nothing..png"), QSize(), QIcon::Normal, QIcon::Off);
        menuExporter->setIcon(icon22);
        menuExporterBalises = new QMenu(menuFichier);
        menuExporterBalises->setObjectName(QString::fromUtf8("menuExporterBalises"));
        menuExporterBalises->setEnabled(false);
        menuEdition = new QMenu(menuBarre);
        menuEdition->setObjectName(QString::fromUtf8("menuEdition"));
        menuNavigation = new QMenu(menuBarre);
        menuNavigation->setObjectName(QString::fromUtf8("menuNavigation"));
        menuOptions = new QMenu(menuBarre);
        menuOptions->setObjectName(QString::fromUtf8("menuOptions"));
        menuModeConsultation = new QMenu(menuOptions);
        menuModeConsultation->setObjectName(QString::fromUtf8("menuModeConsultation"));
        menuModeRecherche = new QMenu(menuOptions);
        menuModeRecherche->setObjectName(QString::fromUtf8("menuModeRecherche"));
        menuChampLecture = new QMenu(menuOptions);
        menuChampLecture->setObjectName(QString::fromUtf8("menuChampLecture"));
        menuAide = new QMenu(menuBarre);
        menuAide->setObjectName(QString::fromUtf8("menuAide"));
        menuLireTemoignage = new QMenu(menuAide);
        menuLireTemoignage->setObjectName(QString::fromUtf8("menuLireTemoignage"));
        menuRecherche = new QMenu(menuBarre);
        menuRecherche->setObjectName(QString::fromUtf8("menuRecherche"));
        menuChercherArticle = new QMenu(menuRecherche);
        menuChercherArticle->setObjectName(QString::fromUtf8("menuChercherArticle"));
        fenetrePrincipale->setMenuBar(menuBarre);
#ifndef QT_NO_SHORTCUT
        etiquetteNomenclature->setBuddy(zoneListeEntrees);
        etiquetteHistorique->setBuddy(zoneListeHistorique);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(zoneTexteConsultation, boutonConsulter);
        QWidget::setTabOrder(boutonConsulter, boutonModeConsultation);
        QWidget::setTabOrder(boutonModeConsultation, zoneTexteRechercheDansArticle);
        QWidget::setTabOrder(zoneTexteRechercheDansArticle, boutonOccurrenceSuivante);
        QWidget::setTabOrder(boutonOccurrenceSuivante, boutonOccurrencePrecedente);
        QWidget::setTabOrder(boutonOccurrencePrecedente, boutonModeRecherche);
        QWidget::setTabOrder(boutonModeRecherche, navigateurArticle);
        QWidget::setTabOrder(navigateurArticle, boutonReculerHistorique);
        QWidget::setTabOrder(boutonReculerHistorique, boutonAvancerHistorique);
        QWidget::setTabOrder(boutonAvancerHistorique, boutonAfficherFlexion);
        QWidget::setTabOrder(boutonAfficherFlexion, boutonAgrandirTexte);
        QWidget::setTabOrder(boutonAgrandirTexte, boutonReduireTexte);
        QWidget::setTabOrder(boutonReduireTexte, boutonAfficherPleinEcran);
        QWidget::setTabOrder(boutonAfficherPleinEcran, boutonChercherTousLesArticles);
        QWidget::setTabOrder(boutonChercherTousLesArticles, boutonCopier);
        QWidget::setTabOrder(boutonCopier, boutonExporter);
        QWidget::setTabOrder(boutonExporter, boutonImprimer);
        QWidget::setTabOrder(boutonImprimer, boutonChampLecture);
        QWidget::setTabOrder(boutonChampLecture, ongletPrincipal);
        QWidget::setTabOrder(ongletPrincipal, zoneListeFormesFlechies);
        QWidget::setTabOrder(zoneListeFormesFlechies, zoneListeEntrees);
        QWidget::setTabOrder(zoneListeEntrees, ongletNomenclature);
        QWidget::setTabOrder(ongletNomenclature, zoneListeSurvol);
        QWidget::setTabOrder(zoneListeSurvol, zoneListeHistorique);
        QWidget::setTabOrder(zoneListeHistorique, ongletConsultationEtRecherche);

        menuBarre->addAction(menuFichier->menuAction());
        menuBarre->addAction(menuEdition->menuAction());
        menuBarre->addAction(menuRecherche->menuAction());
        menuBarre->addAction(menuNavigation->menuAction());
        menuBarre->addAction(menuOptions->menuAction());
        menuBarre->addAction(menuAide->menuAction());
        menuFichier->addAction(menuExporter->menuAction());
        menuFichier->addAction(menuExporterBalises->menuAction());
        menuFichier->addSeparator();
        menuFichier->addAction(actionImprimer);
        menuFichier->addAction(actionAfficherApercuImpression);
        menuFichier->addSeparator();
        menuFichier->addAction(actionQuitter);
        menuExporter->addAction(actionExporterHTML);
        menuExporter->addAction(actionExporterODF);
        menuExporter->addAction(actionExporterPDF);
        menuExporter->addAction(actionExporterTXT);
        menuExporterBalises->addAction(actionExporterBalises);
        menuEdition->addAction(actionCopier);
        menuEdition->addAction(actionToutSelectionner);
        menuNavigation->addAction(actionAfficherFlexion);
        menuNavigation->addSeparator();
        menuNavigation->addAction(actionReculerEntrees);
        menuNavigation->addAction(actionAvancerEntrees);
        menuNavigation->addSeparator();
        menuNavigation->addAction(actionReculerHistorique);
        menuNavigation->addAction(actionAvancerHistorique);
        menuOptions->addAction(actionAgrandirTexte);
        menuOptions->addAction(actionReduireTexte);
        menuOptions->addSeparator();
        menuOptions->addAction(actionAfficherPleinEcran);
        menuOptions->addSeparator();
        menuOptions->addAction(menuModeConsultation->menuAction());
        menuOptions->addAction(menuModeRecherche->menuAction());
        menuOptions->addAction(menuChampLecture->menuAction());
        menuOptions->addSeparator();
        menuOptions->addAction(actionChangerTypographie);
        menuModeConsultation->addAction(actionNegligerAccents);
        menuModeConsultation->addAction(actionDefilerAFurEtAMesure);
        menuModeRecherche->addAction(actionRespecterCasse);
        menuModeRecherche->addAction(actionEmployerExpressionRationnelle);
        menuChampLecture->addAction(actionInclurePrononciation);
        menuChampLecture->addAction(actionInclureNature);
        menuChampLecture->addAction(actionInclureCitations);
        menuChampLecture->addAction(actionInclureHistorique);
        menuChampLecture->addAction(actionInclureEtymologie);
        menuAide->addAction(actionLireCauserie);
        menuAide->addAction(actionLirePreface);
        menuAide->addAction(menuLireTemoignage->menuAction());
        menuAide->addSeparator();
        menuAide->addAction(actionAfficherPortrait);
        menuAide->addSeparator();
        menuAide->addAction(actionAProposDuLogiciel);
        menuLireTemoignage->addAction(actionLireTemoignagePasteur);
        menuLireTemoignage->addAction(actionLireTemoignageRenan);
        menuLireTemoignage->addAction(actionLireTemoignageZola);
        menuRecherche->addAction(menuChercherArticle->menuAction());
        menuRecherche->addSeparator();
        menuRecherche->addAction(actionChercherTousLesArticles);
        menuChercherArticle->addAction(actionOccurrenceSuivante);
        menuChercherArticle->addAction(actionOccurrencePrecedente);

        retranslateUi(fenetrePrincipale);
        QObject::connect(zoneTexteConsultation, SIGNAL(returnPressed()), actionConsulter, SLOT(trigger()));
        QObject::connect(actionQuitter, SIGNAL(triggered()), fenetrePrincipale, SLOT(close()));
        QObject::connect(actionToutSelectionner, SIGNAL(triggered()), navigateurArticle, SLOT(selectAll()));
        QObject::connect(actionAgrandirTexte, SIGNAL(triggered()), navigateurArticle, SLOT(zoomIn()));
        QObject::connect(actionReduireTexte, SIGNAL(triggered()), navigateurArticle, SLOT(zoomOut()));
        QObject::connect(boutonConsulter, SIGNAL(clicked()), actionConsulter, SLOT(trigger()));

        ongletConsultationEtRecherche->setCurrentIndex(0);
        ongletPrincipal->setCurrentIndex(0);
        ongletNomenclature->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(fenetrePrincipale);
    } // setupUi

    void retranslateUi(QMainWindow *fenetrePrincipale)
    {
        fenetrePrincipale->setWindowTitle(QApplication::translate("fenetrePrincipale", "Dictionnaire le Littr\303\251", nullptr));
        actionConsulter->setText(QApplication::translate("fenetrePrincipale", "Consulter", nullptr));
        actionImprimer->setText(QApplication::translate("fenetrePrincipale", "&Imprimer l'article...", nullptr));
#ifndef QT_NO_SHORTCUT
        actionImprimer->setShortcut(QApplication::translate("fenetrePrincipale", "Ctrl+P", nullptr));
#endif // QT_NO_SHORTCUT
        actionQuitter->setText(QApplication::translate("fenetrePrincipale", "&Quitter", nullptr));
        actionCopier->setText(QApplication::translate("fenetrePrincipale", "&Copier le texte entier", nullptr));
        actionToutSelectionner->setText(QApplication::translate("fenetrePrincipale", "&Tout s\303\251lectionner", nullptr));
        actionOccurrenceSuivante->setText(QApplication::translate("fenetrePrincipale", "l'occurrence &suivante", nullptr));
#ifndef QT_NO_TOOLTIP
        actionOccurrenceSuivante->setToolTip(QApplication::translate("fenetrePrincipale", "Chercher l'occurrence suivante", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionOccurrenceSuivante->setShortcut(QApplication::translate("fenetrePrincipale", "F3", nullptr));
#endif // QT_NO_SHORTCUT
        actionOccurrencePrecedente->setText(QApplication::translate("fenetrePrincipale", "l'occurrence &pr\303\251c\303\251dente", nullptr));
#ifndef QT_NO_TOOLTIP
        actionOccurrencePrecedente->setToolTip(QApplication::translate("fenetrePrincipale", "Chercher l'occurrence pr\303\251c\303\251dente", nullptr));
#endif // QT_NO_TOOLTIP
        actionChercherTousLesArticles->setText(QApplication::translate("fenetrePrincipale", "C&hercher dans tous les articles...", nullptr));
#ifndef QT_NO_SHORTCUT
        actionChercherTousLesArticles->setShortcut(QApplication::translate("fenetrePrincipale", "F2", nullptr));
#endif // QT_NO_SHORTCUT
        actionReculerHistorique->setText(QApplication::translate("fenetrePrincipale", "Rec&uler dans l'historique", nullptr));
#ifndef QT_NO_SHORTCUT
        actionReculerHistorique->setShortcut(QApplication::translate("fenetrePrincipale", "Alt+Left", nullptr));
#endif // QT_NO_SHORTCUT
        actionAvancerHistorique->setText(QApplication::translate("fenetrePrincipale", "Avancer &dans l'historique", nullptr));
#ifndef QT_NO_SHORTCUT
        actionAvancerHistorique->setShortcut(QApplication::translate("fenetrePrincipale", "Alt+Right", nullptr));
#endif // QT_NO_SHORTCUT
        actionReculerEntrees->setText(QApplication::translate("fenetrePrincipale", "Re&culer dans les entr\303\251es", nullptr));
#ifndef QT_NO_SHORTCUT
        actionReculerEntrees->setShortcut(QApplication::translate("fenetrePrincipale", "F4", nullptr));
#endif // QT_NO_SHORTCUT
        actionAvancerEntrees->setText(QApplication::translate("fenetrePrincipale", "A&vancer dans les entr\303\251es", nullptr));
#ifndef QT_NO_SHORTCUT
        actionAvancerEntrees->setShortcut(QApplication::translate("fenetrePrincipale", "F5", nullptr));
#endif // QT_NO_SHORTCUT
        actionAgrandirTexte->setText(QApplication::translate("fenetrePrincipale", "Agrand&ir le texte", nullptr));
#ifndef QT_NO_SHORTCUT
        actionAgrandirTexte->setShortcut(QApplication::translate("fenetrePrincipale", "Ctrl++", nullptr));
#endif // QT_NO_SHORTCUT
        actionReduireTexte->setText(QApplication::translate("fenetrePrincipale", "R\303\251&duire le texte", nullptr));
#ifndef QT_NO_SHORTCUT
        actionReduireTexte->setShortcut(QApplication::translate("fenetrePrincipale", "Ctrl+-", nullptr));
#endif // QT_NO_SHORTCUT
        actionAProposDuLogiciel->setText(QApplication::translate("fenetrePrincipale", "S'&informer \303\240 propos du logiciel...", nullptr));
        actionLirePreface->setText(QApplication::translate("fenetrePrincipale", "&Lire la pr\303\251face au dictionnaire", nullptr));
        actionAfficherPleinEcran->setText(QApplication::translate("fenetrePrincipale", "Affic&her en plein \303\251cran", nullptr));
#ifndef QT_NO_SHORTCUT
        actionAfficherPleinEcran->setShortcut(QApplication::translate("fenetrePrincipale", "F11", nullptr));
#endif // QT_NO_SHORTCUT
        actionLireCauserie->setText(QApplication::translate("fenetrePrincipale", "Lire la &causerie d'\303\211mile Littr\303\251", nullptr));
        actionExporterHTML->setText(QApplication::translate("fenetrePrincipale", "au format &HTML...", nullptr));
#ifndef QT_NO_SHORTCUT
        actionExporterHTML->setShortcut(QApplication::translate("fenetrePrincipale", "Ctrl+S", nullptr));
#endif // QT_NO_SHORTCUT
        actionExporterODF->setText(QApplication::translate("fenetrePrincipale", "au format &OpenDocument...", nullptr));
        actionExporterTXT->setText(QApplication::translate("fenetrePrincipale", "au format &texte simple...", nullptr));
        actionExporterBalises->setText(QApplication::translate("fenetrePrincipale", "au format &XML...", nullptr));
        actionAfficherPortrait->setText(QApplication::translate("fenetrePrincipale", "Afficher le &portrait d'\303\211mile Littr\303\251...", nullptr));
        actionAfficherFlexion->setText(QApplication::translate("fenetrePrincipale", "Afficher la fle&xion...", nullptr));
        actionNegligerAccents->setText(QApplication::translate("fenetrePrincipale", "N\303\251gliger les a&ccents et la c\303\251dille", nullptr));
        actionDefilerAFurEtAMesure->setText(QApplication::translate("fenetrePrincipale", "D\303\251fi&ler \303\240 fur et \303\240 mesure", nullptr));
        actionEmployerExpressionRationnelle->setText(QApplication::translate("fenetrePrincipale", "Employer une e&xpression rationnelle", nullptr));
#ifndef QT_NO_SHORTCUT
        actionEmployerExpressionRationnelle->setShortcut(QApplication::translate("fenetrePrincipale", "F6", nullptr));
#endif // QT_NO_SHORTCUT
        actionRespecterCasse->setText(QApplication::translate("fenetrePrincipale", "&Respecter la casse", nullptr));
        actionChangerTypographie->setText(QApplication::translate("fenetrePrincipale", "Changer les couleurs et la t&ypographie...", nullptr));
#ifndef QT_NO_SHORTCUT
        actionChangerTypographie->setShortcut(QApplication::translate("fenetrePrincipale", "F8", nullptr));
#endif // QT_NO_SHORTCUT
        actionAfficherApercuImpression->setText(QApplication::translate("fenetrePrincipale", "Afficher l'aper\303\247&u avant impression...", nullptr));
        actionExporterPDF->setText(QApplication::translate("fenetrePrincipale", "au format &PDF...", nullptr));
        actionInclurePrononciation->setText(QApplication::translate("fenetrePrincipale", "Inclure la &prononciation", nullptr));
        actionInclureNature->setText(QApplication::translate("fenetrePrincipale", "Inclure la &nature", nullptr));
        actionInclureCitations->setText(QApplication::translate("fenetrePrincipale", "Inclure les c&itations", nullptr));
#ifndef QT_NO_SHORTCUT
        actionInclureCitations->setShortcut(QApplication::translate("fenetrePrincipale", "F7", nullptr));
#endif // QT_NO_SHORTCUT
        actionInclureHistorique->setText(QApplication::translate("fenetrePrincipale", "Inclure l'histori&que", nullptr));
        actionInclureEtymologie->setText(QApplication::translate("fenetrePrincipale", "Inclure\302\240l'\303\251&tymologie", nullptr));
        action_Zola_parle_d_mile_Littr->setText(QApplication::translate("fenetrePrincipale", "&Zola parle d'\303\211mile Littr\303\251", nullptr));
        actionRe_nan_d_mile->setText(QApplication::translate("fenetrePrincipale", "Re&nan d'\303\211mile", nullptr));
        actionLireTemoignagePasteur->setText(QApplication::translate("fenetrePrincipale", "par Lo&uis Pasteur", nullptr));
        actionLireTemoignageRenan->setText(QApplication::translate("fenetrePrincipale", "par Ernest &Renan", nullptr));
        actionLireTemoignageZola->setText(QApplication::translate("fenetrePrincipale", "par \303\211mile &Zola", nullptr));
#ifndef QT_NO_TOOLTIP
        boutonConsulter->setToolTip(QApplication::translate("fenetrePrincipale", "Consulter", nullptr));
#endif // QT_NO_TOOLTIP
        ongletConsultationEtRecherche->setTabText(ongletConsultationEtRecherche->indexOf(pageConsultation), QApplication::translate("fenetrePrincipale", "Cons&ultation", nullptr));
        ongletConsultationEtRecherche->setTabText(ongletConsultationEtRecherche->indexOf(pageRechercheDansArticle), QApplication::translate("fenetrePrincipale", "Recherche &dans l'article", nullptr));
        etiquetteNomenclature->setText(QApplication::translate("fenetrePrincipale", "&Liste de tous les articles", nullptr));
        ongletNomenclature->setTabText(ongletNomenclature->indexOf(pageEntrees), QApplication::translate("fenetrePrincipale", "En&tr\303\251es", nullptr));
        ongletNomenclature->setTabText(ongletNomenclature->indexOf(pageFormesFlechies), QApplication::translate("fenetrePrincipale", "Forme&s fl\303\251chies", nullptr));
        ongletPrincipal->setTabText(ongletPrincipal->indexOf(pageNomenclature), QApplication::translate("fenetrePrincipale", "No&menclature", nullptr));
        etiquetteSurvol->setText(QApplication::translate("fenetrePrincipale", "Liste des sens de l'article", nullptr));
        ongletPrincipal->setTabText(ongletPrincipal->indexOf(pageSurvol), QApplication::translate("fenetrePrincipale", "Sur&vol", nullptr));
        etiquetteHistorique->setText(QApplication::translate("fenetrePrincipale", "&Liste des articles consult\303\251s", nullptr));
#ifndef QT_NO_STATUSTIP
        zoneListeHistorique->setStatusTip(QApplication::translate("fenetrePrincipale", "Cliquez pour consulter un article", nullptr));
#endif // QT_NO_STATUSTIP
        ongletPrincipal->setTabText(ongletPrincipal->indexOf(pageHistorique), QApplication::translate("fenetrePrincipale", "&Historique", nullptr));
#ifndef QT_NO_TOOLTIP
        boutonExporter->setToolTip(QApplication::translate("fenetrePrincipale", "Exporter l'article", nullptr));
#endif // QT_NO_TOOLTIP
        menuFichier->setTitle(QApplication::translate("fenetrePrincipale", "&Fichier", nullptr));
        menuExporter->setTitle(QApplication::translate("fenetrePrincipale", "&Exporter l'article", nullptr));
        menuExporterBalises->setTitle(QApplication::translate("fenetrePrincipale", "Exporter les &balises de l'article", nullptr));
        menuEdition->setTitle(QApplication::translate("fenetrePrincipale", "&\303\211dition", nullptr));
        menuNavigation->setTitle(QApplication::translate("fenetrePrincipale", "&Navigation", nullptr));
        menuOptions->setTitle(QApplication::translate("fenetrePrincipale", "&Options", nullptr));
        menuModeConsultation->setTitle(QApplication::translate("fenetrePrincipale", "Pr\303\251ciser le mode de cons&ultation", nullptr));
        menuModeRecherche->setTitle(QApplication::translate("fenetrePrincipale", "Pr\303\251ci&ser le mode de recherche", nullptr));
        menuChampLecture->setTitle(QApplication::translate("fenetrePrincipale", "Pr\303\251ciser le cha&mp de lecture", nullptr));
        menuAide->setTitle(QApplication::translate("fenetrePrincipale", "&Aide", nullptr));
        menuLireTemoignage->setTitle(QApplication::translate("fenetrePrincipale", "Lire le &t\303\251moignage rendu \303\240 Littr\303\251", nullptr));
        menuRecherche->setTitle(QApplication::translate("fenetrePrincipale", "&Recherche", nullptr));
        menuChercherArticle->setTitle(QApplication::translate("fenetrePrincipale", "&Chercher dans l'article", nullptr));
    } // retranslateUi

};

namespace Ui {
    class fenetrePrincipale: public Ui_fenetrePrincipale {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLASSEPRINCIPALE_H
