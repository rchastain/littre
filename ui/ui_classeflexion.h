/********************************************************************************
** Form generated from reading UI file 'classeflexion.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLASSEFLEXION_H
#define UI_CLASSEFLEXION_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_fenetreFlexion
{
public:
    QTextBrowser *navigateurFormes;
    QPushButton *boutonFermer;
    QLabel *etiquette;

    void setupUi(QDialog *fenetreFlexion)
    {
        if (fenetreFlexion->objectName().isEmpty())
            fenetreFlexion->setObjectName(QString::fromUtf8("fenetreFlexion"));
        fenetreFlexion->resize(359, 190);
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Serif"));
        font.setPointSize(10);
        fenetreFlexion->setFont(font);
        navigateurFormes = new QTextBrowser(fenetreFlexion);
        navigateurFormes->setObjectName(QString::fromUtf8("navigateurFormes"));
        navigateurFormes->setGeometry(QRect(9, 33, 341, 101));
        boutonFermer = new QPushButton(fenetreFlexion);
        boutonFermer->setObjectName(QString::fromUtf8("boutonFermer"));
        boutonFermer->setGeometry(QRect(229, 143, 121, 38));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icones/X.png"), QSize(), QIcon::Normal, QIcon::Off);
        boutonFermer->setIcon(icon);
        boutonFermer->setIconSize(QSize(30, 30));
        boutonFermer->setAutoDefault(false);
        etiquette = new QLabel(fenetreFlexion);
        etiquette->setObjectName(QString::fromUtf8("etiquette"));
        etiquette->setGeometry(QRect(9, 9, 341, 24));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        etiquette->setFont(font1);
        etiquette->setAlignment(Qt::AlignCenter);
#ifndef QT_NO_SHORTCUT
        etiquette->setBuddy(navigateurFormes);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(boutonFermer, navigateurFormes);

        retranslateUi(fenetreFlexion);
        QObject::connect(boutonFermer, SIGNAL(clicked()), fenetreFlexion, SLOT(close()));

        QMetaObject::connectSlotsByName(fenetreFlexion);
    } // setupUi

    void retranslateUi(QDialog *fenetreFlexion)
    {
        fenetreFlexion->setWindowTitle(QApplication::translate("fenetreFlexion", "F\303\251minins et pluriels", nullptr));
        boutonFermer->setText(QApplication::translate("fenetreFlexion", "&Fermer", nullptr));
        etiquette->setText(QApplication::translate("fenetreFlexion", "Fle&xion", nullptr));
    } // retranslateUi

};

namespace Ui {
    class fenetreFlexion: public Ui_fenetreFlexion {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLASSEFLEXION_H
