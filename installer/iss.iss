; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "Littre"
#define MyAppVersion "1.0"
#define MyAppPublisher "rchastain, totoletoro57200"
#define MyAppURL "https://gitlab.com/rchastain/littre"
#define MyAppExeName "Littre.exe"
#define LittreDirectory "E:\Bureau\littre"

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{951859CF-5270-4567-A4B0-659C883FA61C}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={autopf}\{#MyAppName}
DisableProgramGroupPage=yes
; Remove the following line to run in administrative install mode (install for all users.)
PrivilegesRequired=lowest
PrivilegesRequiredOverridesAllowed=dialog
OutputDir={#LittreDirectory}\installer
OutputBaseFilename=littresetup
SetupIconFile={#LittreDirectory}\icon.ico
Compression=lzma
SolidCompression=yes
WizardStyle=modern

[Languages]
Name: "french"; MessagesFile: "compiler:Languages\French.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "{#LittreDirectory}\bin\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\D3Dcompiler_47.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\libEGL.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\libgcc_s_seh-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\libGLESV2.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\libstdc++-6.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\libwinpthread-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre.qm"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Etat.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Index.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie1.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie10.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie11.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie12.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie13.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie14.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie15.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie16.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie17.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie18.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie19.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie2.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie20.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie21.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie22.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie23.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie24.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie25.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie26.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie27.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie28.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie29.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie3.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie30.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie4.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie5.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie6.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie7.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie8.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Partie9.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Littre-Prefaces.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\opengl32sw.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Qt5PrintSupport.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Qt5Svg.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#LittreDirectory}\bin\iconengines\*"; DestDir: "{app}\iconengines"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#LittreDirectory}\bin\imageformats\*"; DestDir: "{app}\imageformats"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#LittreDirectory}\bin\platforms\*"; DestDir: "{app}\platforms"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#LittreDirectory}\bin\printsupport\*"; DestDir: "{app}\printsupport"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#LittreDirectory}\bin\styles\*"; DestDir: "{app}\styles"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#LittreDirectory}\bin\translations\*"; DestDir: "{app}\translations"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

