@echo off
REM Ce script doit être lancé dans le terminal QT,
REM ou alors il faut ajouter le chemin des programmes de QT à la variable d'environnement PATH.
cd bin/
qmake ../Littre.pro
make
windeployqt .
REM Création facultative d'un installateur.
set /P c=Voulez-vous creer un installateur ? [Y/n]
if /I "%c%" EQU "N" exit
cd ../installer
iscc iss.iss
