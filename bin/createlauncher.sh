## Création d'un lanceur sur le bureau (Linux)

## Répertoire du script
D1="$(dirname "$(readlink -f "$0")")"

## Répertoires de destination
D2="$HOME/Desktop"
D3="/usr/share/applications"

if [ ! -d $D2 ]; then
  D2="$HOME/Bureau"
fi

if [ ! -d $D2 ]; then
  D2=$(xdg-user-dir DESKTOP)
fi

if [ -d $D2 ] ;
then
  FILE=$D2/littre.desktop
  echo "INFO Création du fichier $FILE"
  cat > $FILE << EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=Littré
Comment=Dictionnaire de la langue française
Exec=$D1/Littre %F
Icon=$D1/Littre.ico
Path=$D1
Terminal=false
StartupNotify=true
Categories=Office;Dictionary;
EOF
  echo "INFO Permission d'exécuter"
  sudo chmod -R 777 $FILE
  if [ -d $D3 ] ;
  then
    FILE2=$D3/eschecs.desktop
    echo "INFO Copie de $FILE vers $FILE2"
    sudo cp -f $FILE $FILE2
  else
    echo "ERREUR Impossible de trouver le répertoire $D3"
  fi
  echo "INFO Opération terminée"
else
  echo "ERREUR Impossible de trouver le répertoire $D2"
fi
