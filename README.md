# Dictionnaire Littré

Logiciel permettant de consulter le dictionnaire de la langue française d'Émile Littré.

## Capture d'écran

![Capture d'écran](./screenshots/screenshot.png)

## Téléchargement

La dernière version de l'application peut être téléchargée sur [cette page](https://sourceforge.net/projects/dictionnaire-littre/files/).

L'application est disponible pour Linux et pour Windows.

## Compilation

Si vous souhaitez compiler vous-même l'application, vous avez besoin de Qt Creator. Vous pouvez alors soit ouvrir le projet *Littre.pro* dans Qt Creator, soit utiliser des lignes de commandes.

Les commandes suivantes permettent de télécharger le code source, de le compiler, d'installer l'application et de la lancer sous Linux.

```
git clone https://gitlab.com/rchastain/littre.git
cd littre
qmake ./Littre.pro
make
chmod +x ./bin/Littre
sudo make install
Littre
```

Pour les commandes sous Windows, voyez le script [build.bat](https://gitlab.com/rchastain/littre/-/blob/master/build.bat).

## Histoire

Le logiciel est l'œuvre de Murielle Descerisiers.

La version originale, pour Qt 4, est disponible sur [cette page](https://code.google.com/archive/p/dictionnaire-le-littre/).

La version disponible [ici](https://gitlab.com/rchastain/littre) est basée sur Qt 5. Elle est maintenue par Tom Nirrengarten et Roland Chastain.
